from django.http import HttpResponseForbidden
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import ProjectForm


# Create your views here.
@login_required
def list_projects(request):
    name = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": name,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    if not project.owner == request.user:
        return HttpResponseForbidden("Access Denied")
    context = {
        "project_object": project,
    }
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")

    else:
        form = ProjectForm()
        context = {
            "form": form,
        }
        return render(request, "projects/create.html", context)

@login_required
def edit_project(request, id):
    project = get_object_or_404(Project, id=id)
    if not project.owner == request.user:
        return HttpResponseForbidden("Access Denied")
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project)
        if form.is_valid():
            form.save()
            return redirect("show_project", id=id)
    else:
        form = ProjectForm(instance=project)
    context = {
        "form": form,
    }
    return render(request, "projects/edit.html", context)
