from django.http import HttpResponseForbidden
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.instance.owner = request.user
            form.save()
            return redirect("create_task")

    else:
        form = TaskForm()
        context = {
            "form": form,
        }
        return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    project = Task.objects.filter(assignee=request.user)
    context = {
        "project_object": project,
    }
    return render(request, "tasks/list.html", context)

@login_required
def task_detail(request, id):
    tasks = get_object_or_404(Task, id=id)
    if not tasks.assignee == request.user:
        return HttpResponseForbidden("Access Denied")
    context = {
        "tasks_object": tasks,
    }
    return render(request, "tasks/details.html", context)

@login_required
def edit_task(request, id):
    tasks = get_object_or_404(Task, id=id)
    if not tasks.assignee == request.user:
        return HttpResponseForbidden("Access Denied")
    if request.method == "POST":
        form = TaskForm(request.POST, instance=tasks)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskForm(instance=tasks)
        context = {
            "form": form,
        }
        return render(request, "tasks/edit.html", context)

@login_required
def delete_task(request, id):
    task = Task.objects.get(id=id)
    if not task.assignee == request.user:
        return HttpResponseForbidden("Access Denied")

    if request.method == "POST":
        task.delete()
        return redirect("show_my_tasks")

    return render(request, "tasks/delete.html", {'task': task})
