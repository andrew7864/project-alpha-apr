from django.urls import path
from tasks.views import create_task, show_my_tasks, task_detail, edit_task, delete_task

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("mine/<int:id>", task_detail, name="task_detail"),
    path("mine/<int:id>/edit", edit_task, name="edit_task"),
    path("mine/<int:id>/delete", delete_task, name="delete_task"),
]
